---
layout: markdown_page
title: Product Direction - Expansion
description: The expansion group at GitLab focuses on the upgrade and add-on workflows for existing customers
---

## Overview

The Expansion Team at GitLab focuses on running experiments to increase the expansion of our platform and expanding usage by teams within an organization or by individual users additionally we strive to increase the value of Gitlab to existing customers getting them to adopt new features in higher paid tiers. It's easy to look at GitLab and quickly come to a conclusion that our platform is for technical teams only (e.g. developers and engineers) but in fact many other non-technical teams can use GitLab to increase efficiency and integrate their work into a company's development life-cycle. To call out a few Product Managers, Data Analysts, Marketers, Support, UI/UX and Executives use GitLab in their day-to-day. We want to get these teams to that ‘ah-Ha!’ moment so they can also benefit from using the platform. Our experiments are all public and we encourage feedback from the community and internal team. 

### Expansion Team

Product Manager: [Tim Hey](/company/team/#timhey) | 
Engineering Manager: [Phil Calder](/company/team/#pcalder) | 
UX Manager: [Jacki Bauer](/company/team/#jackib) | 
Product Designer: [Matej Latin](/company/team/#matejlatin) | 
Full Stack Engineer: [Doug Stull](/company/team/#dougstull) | 
Full Stack Engineer: [Jackie Fraser](/company/team/#jackie_fraser)

### Expansion Team Mission

*   Expand ARR for current customers by driving seat expansion, upgrades, add ons, reduced discounts, etc.

### Expansion KPI

*   [Net retention](https://about.gitlab.com/handbook/customer-success/vision/#financial-kpis)

_Supporting performance indicators:_

*   (#) of seats added 
*   % of seats added
*   Seats by stage 
*   Seats added by tier
*   Session level Usage (ping data)
*   (#) Seats by territory (where in the world)
*   (#) Seats by buyer Persona type ([buyer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#buyer-personas) persona)
*   User Persona type ([user](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#user-personas) persona)
*   Source of user acquisition (use channel e.g. paid, earned, social, owned)
*   Account usage
*   Seat utilization by account (how many seats did they sign up for and how many are they using)
*   % of instances (and groups, for GitLab.com) using more than ‘x’ stages 
*   MAU - For all stages - The number of unique users who had at least one session within a 28-day period. The 28-day period includes the last day in the active date range.
*   SMAU - Monthly active users by stage - this will be a subset of MAU at the stage level
*   SMAU Factor - expressed as the average number of stages used by users
*   Feature MAU - Monthly active users by feature - this will be a subset of SMAU
*   Feature MAU Factor - expressed as the average number of features by SMAU
*   Usage Pings - How many ping counts were created by all users
*   User Count Free - How many users were in the 'Free' plan
*   User Count Bronze - How many users were in the 'Bronze' plan
*   User Count Silver - How many users were in the 'Silver' plan
*   User Count Gold - How many users were in the 'Gold' plan
*   Total User Count - How many total users who have an account with GitLab

### Our approach

Expansion runs a standard growth process. We gather feedback, analyze the information, ideate then create experiments and test. As you can imagine the amount of ideas we can come up with is enormous so we use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64) (impact, confidence, effort) to ensure we are focusing on the experiments that will provide the most value. 

### Maturity

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the expansion group and are planning to become a more mature, organized and well oiled machine by January 2020. 

### Problems we solve

Do you have issues... We’d love to hear about them, how can we help GitLab contributors find that ah-Ha! Moment. 

Here are few problems we are trying to solve: 

*   **User Orientation** - Users don’t know where to start
    *   As a product manager, where do I start?
    *   What are the best tools for me to use on this platform as a security engineer? 
*   **Increase platform confidence and trust** - I love my tools and am afraid to switch
    *   We know our users love the tools they are currently using and that change is hard. We are trying to earn your trust and show you how to use our platform as one solution.
*   **Visibility** - Ever get that email, you know the one “can you give me an update on…” when individual contributors, management and executives are all on the same platform visibility increases 10 fold. 
    *   The more teams from your company using GitLab will dramatically improve visibility by reducing friction and noise and help you stay focused. 

### What user workflows does the expansion team focus on?

The user workflows on GitLab.com and our Self-managed instances are very different and should be treated separately. We will be focusing on the 2 described below. *note: our sales team outlines the process in [point 3. on this page](https://about.gitlab.com/handbook/business-ops/resources/#opportunity-types). (Again the sales motion is very different than the self serve experience on GitLab.com).*

*  **Add-on purchases** - ci minutes ([dashboard](https://app.periscopedata.com/app/gitlab/434763/CI-Minutes)), seats etc. Note: seat adds for expansion happen outside the renewal process think of opportunities to add more users through projects and groups vs. adding users during renewal. 

*  **Tier upgrades** - The movement of customers from tier to tier can be found in this [dashboard](https://app.periscopedata.com/app/gitlab/484507/Churn-%7C-Expansion-by-Sales-Segment). Keep in mind we're focused on customers who are already paying subscribers no free to paid. Here is the [UX Scorecard for upgrading accounts](https://gitlab.com/gitlab-org/growth/product/issues/113)

### Key User Focus:

#### External
The external user personas on GitLab.com and our Self-Managed instances are very different and should be treated as such. We will be focusing on the 2 described below. 

In addition to the personas, it's also important to understand the permissions available for users (limits and abilities) at each level. 
* [Handbook page on permissions in GitLab](https://about.gitlab.com/handbook/product/#permissions-in-gitlab)
* [Docs on permission on docs.gitlab.com](https://docs.gitlab.com/ee/user/permissions.html)
* *Note: GitLab.com and Self-Managed permission do differ.*  

#### GitLab Team Members
*  Sales Representatives
*  Customer Success Representatives
*  Support Representatives

### Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [Customers.GitLab](https://about.gitlab.com/handbook/engineering/projects/#customers-app)
*   [License.GitLab](https://about.gitlab.com/handbook/engineering/projects/#license-app)

### Helpful Links
*   [Expansion Team Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1370834?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aexpansion)
*   [Expansion Epic](https://gitlab.com/groups/gitlab-org/-/epics/2358)
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
