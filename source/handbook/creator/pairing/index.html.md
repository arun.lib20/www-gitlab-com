---
layout: markdown_page
title: "Creator pairing"
---

## Introduction

Creator pairing is a program that allows any GitLab engineer to work together with GitLab co-founder Dmitriy Zaporozhets (DZ) from the same physical location for a week.

## Goal

The goal of the program is to help DZ during his work as a fellow of initial delight.
As an additional benefit, a co-worker will learn about how DZ thinks and sees the product.

## Things to Know

* You need to travel to DZ location in Kharkiv, Ukraine or Limassol, Cyprus.
* The duration is one week per person.
* Don't plan to do any of your usual work. Tell your team you're on paid time off.
* The company will pay for travel and hotel.

## Who can apply

Any [senior or higher](/handbook/engineering/career-development/#engineering) backend or frontend engineer at GitLab.

## How to apply

1. Create a merge request to add yourself to the [schedule](#schedule).
1. Ask your manager to approve (but not merge) the merge request.
1. Assign the merge request to DZ (`@dzaporozhets`).

## Schedule

| Start date | End date | Who | Location | Results |
|---|---|---|---|--- |
| 2019-11-04 | 2019-11-08 | [Tetiana Chupryna ](https://gitlab.com/brytannia) | Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1crPEyjISN03zqV0HTjCCjI419SNGRH1sU0xxkglGHFM/edit?usp=sharing) |
| 2019-12-09 | 2019-12-13 | [Sam Beckham](https://gitlab.com/samdbeckham)  | Limassol, Cyprus | [Video Interview](https://www.youtube.com/watch?v=lVKqsB2gePU&feature=youtu.be), [Document](https://docs.google.com/document/d/1CnlwGIGvlzXmLq4Fy1iLNvFedEC1EQI2y0t_AARDfMU/edit) |
| 2019-12-20 | 2020-01-25 | `NOT AVAILABLE` | N/A |
| 2020-01-27 | 2020-01-31 | [Imre Farkas](https://gitlab.com/ifarkas) | Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1LGtf6W1ESzr8U_DDzgvh7AoFfu3yBVTWVo9gNQcvx10/edit) |
| 2020-02-03 | 2020-02-07 | [Jason Plum](https://gitlab.com/warheadsse) | Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1yR0oGiPXsQYUNlW4F5SKy8co1oOwCSBzHiMsuBovRF4/edit) |
| 2020-02-24 | 2020-02-28 | [Christian Couder](https://gitlab.com/chriscool) |  Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1b-z1awjdqkh7s_cvdMbuXxd-U8jwdGYyGxSfhCTLDwM/edit?usp=sharing) |
| 2020-03-09 | 2020-03-13 | [Natalia Tepluhina](https://gitlab.com/ntepluhina) | Kharkiv, Ukraine |
| 2020-04-13 | 2020-04-16 | AVAILABLE | TO BE DECIDED |
| 2020-04-27 | 2020-04-30 | [Ian Baum](https://gitlab.com/ibaum) |  Limassol, Cyprus |
| 2020-05-11 | 2020-05-14 | [Douglas Barbosa Alexandre](https://gitlab.com/dbalexandre) | Limassol, Cyprus |
| 2020-06-15 | 2020-06-18 | [Arturo Herrero](https://gitlab.com/arturoherrero) | TO BE DECIDED |
| 2020-07-13 | 2020-07-16 | AVAILABLE | TO BE DECIDED |
| 2020-07-27 | 2020-07-30 | AVAILABLE | TO BE DECIDED |
| 2020-08-03 | 2020-08-06 | AVAILABLE | TO BE DECIDED |
| 2020-08-25 | 2020-08-28 | AVAILABLE | TO BE DECIDED |
| 2020-09-14 | 2020-09-17 | AVAILABLE | TO BE DECIDED |

## FAQ

- Q: Is a visa required to travel to Ukraine?

  A: [Ministry of Foreign Affairs of Ukraine](https://mfa.gov.ua/en/consular-affairs/entering-ukraine/visa-requirements-for-foreigners) can advise if a visa is necessary based on your country of origin.

- Q: Who books travel?

  A: In most cases, the participant will book their own flight to Ukraine or Cyprus, but the EBA to VP, Engineering is also available to assist and coordinate.

- Q: What airport should participants fly in to?

  A: Once manager approval has been received and the MR has been merged, DZ will add the location in which the rotation will occur. For Kharkiv the recommended airport is HRK. For Cyprus it is LCA.

- Q: Who will book hotel or accommodations for the rotation?

  A: The [EBA to the VP, Engineering](/handbook/eba/#executive-business-administrator-team) can book a hotel or AirBnB on behalf of the participants.  If a participant would like to book their own accommodations that is perfectly acceptable; please communicate in the #creator-pairing Slack channel to advise.

- Q: What area I should look to book accommodations?

  A: You will work from a coworking place with DZ. For Kharkiv it will be [Fabrika.space](https://goo.gl/maps/CZ3TkekKCm4uRt7T9). For Limassol it will be [The Business Bar](https://goo.gl/maps/GxoNc6inGaC6MCuy9). We recommend you to find an accommodation not too far away from it.

If you have questions, you should post in the [#creator-pairing](https://gitlab.slack.com/messages/CPD077F7V) channel on Slack.






