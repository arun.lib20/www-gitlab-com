---
layout: handbook-page-toc
title: "Sales Development"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

As a Sales Development Representative (SDR), you focus on outreach, prospecting, and lead qualification. To do so, you need to have an understanding of not only product and industry knowledge, but also sales soft skills, and internal tools and processes. This handbook page will act as a guide to those topics as well as general information about the SDR team.

## Reaching the Sales Development Team (internally)

#### Slack Channels

* **Main Channel** = [`#sdr_global`](https://gitlab.slack.com/messages/C2V1KLY0Z)      
* **Enablement** = [`#sdr-enablement`](https://gitlab.slack.com/messages/CCV9KKAQY)       
* **Conversations** = [`#sdr-conversations`](https://gitlab.slack.com/messages/CD6NDT44F)       

##### AMER
* **All** = [`#sdr_amer`](https://gitlab.slack.com/messages/CM2GAVC78)      
* **Commercial + LATAM** = [`#sdr_amer_commercial`](https://gitlab.slack.com/messages/CM276FD2B)        
* **East** = [`#sdr_amer_east`](https://gitlab.slack.com/messages/CGTF184EB)      
* **West** = [`#sdr_amer_west`](https://gitlab.slack.com/messages/CLU1A6BA5)
* **Named** = [`#sdr_amer_named`](https://app.slack.com/client/T02592416/CUFRP6U6Q)

##### EMEA
* **All** = [`#sdr_emea`](https://gitlab.slack.com/messages/CCULKLB71)      
* **Commercial** = [`#sdr_emea_commercial`](https://gitlab.slack.com/messages/CM0BYV7CM)      

##### APAC
* **All** = [`#sdr_apac`](https://gitlab.slack.com/messages/CM0BPBEQM)

#### Issue Boards & Team Labels 

SDRs use the [SDR Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/707128) to track relevant GitLab issues involving the SDR team. This is a global issue board and will capture all issues in any group/sub-group in the GitLab.com repo when any of the following *scoped* labels are used. 

- `SDR` - issues concerning the SDR team
- `SDR::Priority` - projects that we would like brought into RevOps meeting for feedback/next steps from other teams
- `SDR::Review` - issues concerning the SDR team
- `SDR::Planning` - Discussion about next steps is in progress for issues concerning the SDR team
- `SDR::On Deck` - Action item for the SDR team has been determined and is on deck to be completed
- `SDR::In Progress` - SDR action item is presently being worked on
- `SDR::On Hold` - SDR project is put on hold after agreement from SDR leadership team 
- `SDR::Watching` - No direct SDR action item at this time, but SDR awareness is needed for potential support/questions
- `SDR::AMER Event Awareness` - Americas SDR awareness is needed for potential support/questions in regards to events
- `SDR::APAC Event Awareness` - EMEA SDR awareness is needed for potential support/questions in regards to events
- `SDR::EMEA Event Awareness` - APAC SDR awareness is needed for potential support/questions in regards to events

## Onboarding
In your first month at GitLab we want to help ensure you have everything you need to be successful in your job. You will go through enablement videos, live sessions and activities covering a wide range of getting started topics. 
- [SDR onboarding goals and process](/handbook/marketing/revenue-marketing/sdr/sdr-playbook-onboarding/)

## Segmentation
The SDR team aligns to the [Commercial](handbook/sales/commercial/) and Large sales teams. These teams are broken down into three segments: Large, Mid-Market and SMB which are based on the total employee count of the Global account. *Note: The commercial sales team includes both Mid-Market and SMB. This segmentation allows SDRs and Sales to be targeted in their approach and messaging. The segments are aligned to a region/vertical and then divided one step further via territories in the regions. Our single source of truth for determining number of employees is DataFox followed by DiscoverOrg. 
* [Sales segmentation](/handbook/business-ops/resources/#segmentation)
* [Sales territories](/handbook/sales/territories/)
* [Determining if a lead is in your territory](/handbook/business-ops/resources/#account-ownership-rules-of-engagement)
* [SDR and Sales alignment](/handbook/marketing/revenue-marketing/sdr/sales-sdr-alignment/) - working with your SAL or AE!

**SDR Team Breakdown**
Geo = SDR teams that align directly to sales territories and AE/SALs that work a geo territory
     *  Commercial (MM & SMB)
     *  West Enterprise
     *  East Enterprise
Named = SDR team that supports only Named SALs/Accounts
Acceleration = aligns to the Large sales team but is only in certain territories for a temporary amount of time based on need. 

## SDR Compensation and Quota

**Quota**

You will have a monthly quota that is based soley on [Sales Accepted Opportunities (SAOs)](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao). During the Ramp Period (Months 1-2) once you have achieved quota, each additional opportunity is paid at the Month 3 base rate.
*  Month 1 - 0% SAOs 
    *  During your first month of employment you will be paid your OTE Commission by completing the GitLab SDR Onboarding assessments and issue. 
*  Month 2 - 50% SAOs
*  Month 3 onwards - 100% SAOs  

**Accelerator** 

If you exceed your SAO quota, there is an accelerator based on progressive tiers of quota attainment:

* Base tier, 0-100%: Base Rate Payment (Monthly Variable @ Target/quota)
* Accelerator 1, 101%-150%: 1.1 times Base Rate Payment in excess of Quota
* Accelerator 2, > 150%: 1.5 times Base Rate Payment of quota

**Closed won business from SAOs** 

As an SDR at GitLab, you will receive 1% commission for any closed won opportunity you produce. To be paid on your closed won business, your activity should clearly depict that your prospecting work sourced the opportunity.

**SAO Salesforce Reports**
* [Outbound SDR created opportunities](https://gitlab.my.salesforce.com/00O61000003nmhe)
* [Outbound SDR sales accepted opportunities](https://gitlab.my.salesforce.com/00O61000003nmhU?dbw=1)
*If you believe an opportunity you sourced is not reflected in the reporting, notify your manager.

**Activity & Results Metrics**

While the below measurements do not impact your quota attainment, they are monitored by SDR leadership. 
* Results
  * Pipeline value of SDR sourced opportunities
  * IACV won from opportunities SDR sources
* Activity
  * % of named accounts contacted
  * Number of opportunities created
  * Number of calls made
  * Number of personalized emails sent
  * Daily outbound metrics 
    * At least 30 emails/day
    * At least 30 dials/day

## SDR Tools
* Salesforce
* [Outreach.io](/handbook/marketing/marketing-operations/outreach/)
* DataFox
* DiscoverOrg
* LinkedIn Sales Navigator
* Chorus
* [Drift](/handbook/marketing/marketing-operations/drift/)
* [LeanData](/handbook/marketing/marketing-operations/leandata/)

## SDR Workflow & Process
As an SDR, you will be focused on leads - both inbound and outbound. At the highest level, a lead is a person who shows interest in GitLab through inbound lead generation tactics or through outbound prospecting.

**Inbound Lead Generation**

Inbound lead generation often uses digital channels - social media, email, mobile/web apps, search engines, websites, etc - as well as in-person marketing activities to meet potential buyers where they are. When people interact with GitLab, we use lead scoring to assign a numerical value, or points, to each of these leads based on their actions and the information we have about them. Once a lead reaches 90 points, they are considered a [Marketo Qualified Lead](/handbook/business-ops/resources/#mql-definition) or MQL.

**Working Inbound Leads**

SDRs are responsible for following up with MQLs by reviewing their information, reaching out, and working with them to understand their goals, needs, and problems. Once you have that information, you can use our [qualification criteria](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao) to determine if this is someone who has strong potential to purchase our product and therefore should be connected with sales for next steps. As you are the connection between Marketing and Sales you want to make sure every lead you pass to the Sales team is as qualified as possible.

These MQLs will show up in your lead views in Salesforce. The views, listed below, allow you to see your leads in a categorized way to simplify your workflow. Leads are routed to you and flow into your views via the tool [LeanData](/handbook/marketing/marketing-operations/leandata/) which takes each lead through a series of conditional questions to ensure it goes to the right person. Even though all SDRs leverage the same views, they will only show you leads that have been specifically routed to you. You will be responsible for following up with all of the leads in your MQL views by sequencing them. Once sequenced, their lead status will change and they will move from your MQL views allowing you to have an empty view.

**Lead Views**
* My MQLs - all leads that need to be reached out to
* My Inquiries - leads that aren't an MQL yet but have taken some actions
* My Qualifying - leads that you are actively qualifying in a back and forth conversation

**Contact Views**

You  also have two contact views. COMING SOON! Our end goal is to route contacts to these contact views if someone is already a contact in our system but has taken a lead qualifying action or requested contact. After a certain amount of time, the record ownership will transition back to SAL/AE ownership.
*  My MQLs - all contacts that have taken a recent lead qualifying action. Determine if this contact is closely engaged with Sales and reach out accordingly
*  My Qualifying - contacts you are actively qualifying in a back and forth conversation

**Lead and Contact Status**

Lead/contact statuses allow anyone in Salesforce to understand where a lead is at in their journey. 

* Once a lead has been placed in a sequence, the lead status will automatically change from MQL, Inquiry or Raw to Accepted marking that you are actively working this lead. 
* When a lead responds to you via email, their status will again automatically change. This time it will change from Accepted to Qualifying. *If you are not working on qualifying this lead further, you will need to manually change the status to Nurture so that this lead is back in Marketing nurture and isn’t stuck in your qualifying view. 
* If a lead finishes a sequence without responding, the lead status will automatically change to unresponsive or nurture in three days if there is still no response.
* The only time you will need to manually change lead status outside of what is mention in the previous bullets is if you for some reason don't use a sequence to reach out to someone or if you need to unqualify a lead for bad data etc.

### Inbound Lead Workflow Checklist

**IMPORTANT:** For Mid Market SDR reps, if the account your lead matches has the account type 'customer,' this lead will need to be worked by your aligned AE. Update the lead owner and let them know. You can determine this via the LeanData section in Salesforce or by leveraging the 'Find Duplicates' button at the top of the lead in Salesforce. Look to see if the account this lead belongs to has a CARR value (total annual recurring revenue this customer brings) associated with it. Lastly, you can [search for the account](/handbook/support/workflows/looking_up_customer_account_details.html#finding-the-customers-organization) to look for the CARR value. 

**Checklist**
* Make sure the lead is in your territory/segmentation
    * [Parent/Child Segmentation:](/handbook/business-ops/resources/#account-ownership-rules-of-engagement) all accounts in a hierarchy will adopt the MAX segmentation of any account in the hierarchy.
    * If the lead is not yours, please route it to the appropriate rep
* Make sure name is capitalized correctly
* Check for duplicates and merge or route to the appropriate rep  - VIDEO COMING SOON
* Add or update any missing information on the lead. The SDR team is responsible for making sure lead data is up to date and as detailed as possible.
    * Check Datafox to confirm and/or add company location, number of employees/employees bucket, company phone number 
        * If there is no Datafox information, use DiscoverOrg and lastly LinkedIn 
    * If you have a company email domain, use LinkedIn to find the lead's job title  
* Has this person opted out of GitLab communication? In order to remain compliant we cannot follow up via phone or email as we don’t have granular opt-out. You need to change lead status to unqualified and unqualified reason to unsubscribe.

**Outreach & Marketo**

If the lead is yours to work based on all of the above, sequence them in Outreach using the master sequence that correlates with the Last Interesting Moment. You can also look at the initial source and Marketo insights to get a holistic view of what a lead is looking into. There is an Outreach collection of sequences for inbound leads for each region. These collections contain a master sequence for nearly every inbound lead scenario. 
* **Master Sequence**: a sequence created by SDR leadership that should be used by all reps to follow up with inbound leads
*  [**Sequence collection**](https://support.outreach.io/hc/en-us/articles/360009145833-Collections): group of sequences compiled by region
*  **Last Interesting Moment**: data pulled in from our marketing auotomation software, [Marketo](/handbook/marketing/marketing-operations/marketo/), that tells you the last action a lead took.
*  [**Initial source**](https://about.gitlab.com/handbook/business-ops/resources/#initial-source): first known action someone took when they entered our database
*  **Marketo Sales Insights (MSI)**: a section on the lead/contact in Salesforce that shows you compiled data around actions a lead/contact has taken
*  **High touch and low touch sequences**: a high touch sequence should be used for high-quality leads. High touch sequences require you to add in more personalization and have more touch points across a longer period of time. Low touch sequneces are typically automated and run for a shorter period of time. A high quality lead has valid information for at least two of the following fields:
    * Company email domain or company name
    * Phone number
    * Title

### Qualification Criteria and SAOs

Qualification criteria is a minimum set of characteristics that a lead must have in order to be passed to sales and become a Sales Accepted Opportunity (SAO). You will work to connect with leads that you get a response from to obtain this information while assisting them with questions or walking them through how GitLab might be able to help with their current needs. The qualification criteria listed in the linked handbook page below aligns to the 'Qualification Questions' sections on the LEAD, CONTACT, and OPPORTUNITY object in Salesforce. In order to obtain an SAO you will need to have the 'required' information filled out on the opportunity.  
*  [Qualification criteria needed](/handbook/business-ops/resources/#criteria-for-sales-accepted-opportunity-sao)

When a lead meets the qualification criteria and you have an IQM/intro call scheduled with your AE/SAL, you will convert the lead to an opportunity. 
* [ How to create an opportunity](/handbook/business-ops/resources/#creating-a-new-business-opportunity-from-lead-record)

### Questions about a lead?

Use slack channel #lead-questions

**Technical questions from leads?**
* [Docs](https://docs.gitlab.com/)
* Slack channels
    * #questions
    * #support_self-managed
    * #support_gitlab-com

### Working with 3rd Parties

1.  Gather billing and end user details from the reseller:
    * Billing company name/address:
    * Billing company contact/email address:
    * End user company name/address:
    * End user contact/email address:
    * [Snippet in outreach](https://app1a.outreach.io/snippets/362)
2. Create a new lead record with end user details 
    *  Ensure that all notes are copied over to new LEAD as this is the LEAD that will be converted.
3. Converting the new lead
    * Name opp to reflect reseller involvement as shown here: “End user account name via reseller account name”
4. Convert original reseller lead to a contact associated with the reseller account
    *  If an account does not already exist for the reseller, create one when converting the lead to a contact.
    *  Assign record to the same account owner
    *  Do NOT create a new opportunity with this lead.
5. Attach activity to the opportunity
    * On the reseller contact, go to the activity and link each activity related to your opportunity to the opp.
        * Activity History > click edit to the left of the activity > choose 'opportunity' from the 'related to' dropdown > find the new opportunity > save
6. Update the opportunity
    * Change the business type to new business and stage to pending acceptance.
    * Under contacts, add the reseller contact, role as reseller, and primary contact.
    * Under partners, add the reseller account as VAR/Reseller"

### Drift Chat Platform
We use a the chat platform Drift to engage site visitors. As an SDR, you are expected to be active on Drift throughout your work day. The [tool's handbook page](/handbook/marketing/marketing-operations/drift/) will walk you through guidlines, best practices and troubleshooting. 

## Outbound Prospecting 

Outbound lead generation is done through prospecting to people who fall into our target audience and could be a great fit for our product. Prospecting is the process of finding and developing new business through searching for potential customers with the end goal of moving these people through the sales funnel until they eventually convert into customers. 

You will work closely with your dedicated SAL or AE to build a strategy for certain companies, parts of your territory or personas you as an aligned team want to target. It is crucial that you are very intentional and strategic in your approach and always keep the customer experience in mind. When reaching out, offer value and become a trusted advisor to ensure we always leave a positive impression whether there is current demand or not.

### Targeted Accounts

**Targeted account**: an account in which you as an SDR have developed an account plan with your AE or SAL and are actively targeting via outbound prospecting.

Targeted Accounts should be kept up-to-date in Salesforce in real-time, and checked as soon as you begin the account planning process. If the account does not have activity associated with leads/contacts over a 30-day period, the account should not be flagged as targeted. Each SDR is allowed 40 targeted accounts at any given time. If you feel this is too low, please connect with your manager.

If you check the targeted checkbox, you are committed to:
* Developing an account plan with your AE or SAL
* Actively outbounding to ideal personas leveraging person

Keeping your targeted accounts up to date is especially crucial when the Acceleration Team is entering your territory. The Acceleration Team typically generates a regional account target list 60-days prior to entering a region. At that point, any SDR target accounts” will be suppressed. Two weeks prior to kickoff, the Acceleration Team will schedule a kickoff call to:
* Review coverage dates
* Review target account list
* Ensure that targeted accounts and regional suppression lists are up-to-date
* Discuss account plans / targeting strategy

### Outbound Workflow
**IMPORTANT**: EMEA reps, old leads/contacts can only be called or emailed if they were in our system from May 2018 or later. Any lead/contact that was brought into our system prior would need to opt in to communication. Any new leads/contacts can only be called or emailed if they have been brought in from DiscoverOrg. You can also leverage LinkedIn Sales Navigator. 

**Step 1: Industry and Customer Profile Knowledge**
* Make sure you have an understanding of our target audience, where GitLab sits in the industry, and who our competitors are.
* SDR training resources
* Sales training resources

**Step 2: Research**
* Connect with your SAL/AE to discuss target accounts and prioritization of those accounts.
* Check Salesforce to determine what involvement we have had with that account to date
    * Any closed won opportunities? If so, what was their use case? Read the opportunity notes.
    * Any closed lost opportunities? Why did the opp not close? Can it be revived? 
    * Are there leads being worked?
    * Are there other accounts with alternative naming conventions?
* Look at company website, blogs and LinkedIn to understand: 
    * Company vision and values
    * Latest IT initiatives
    * Recent updates
    * The careers page may give you some insight as to what tool knowledge they want technical hires to have, giving you a glimpse into their tech stack.
* Leverage version.Gitlab to understand GitLab CE usage at certain companies. 
* Build a list of leads/contacts in our system you could retarget, keeping in mind when we last spoke with them, where the conversation went, and their end experience of being reached out to again.  
* Build a list of leads by leveraging DiscoverOrg and LinkedIn Sales Navigator
    * Keep note of mutal connections, interests, companies etc. in LinkedIn that you can use to persoanlize your message one step further

**Step 3: Strategize and Prioritize**
* If you are targeting multiple accounts, determine if you are taking an account, role, or value driver based approach to your messaging.
    * Account specific: personalized messaging about the company and their initiatives as a whole. This type of messaging should be created per company.
    * Role specific: personalized messaging about common pain points and priorities somone in a specific role or team may have and how GitLab can help those pain points. This type of messaging can be leveraged across multiple accounts.
        * [Roles and personas](handbook/marketing/product-marketing/roles-personas/)
    * Industry/vertical specific: personalized messaging about common themes and pain points for an industry as a whole. Leveraging industry-specific language. This type of messaging can be used across multiple accounts in the same industry or vertical. 
        * [Case studies](https://about.gitlab.com/customers/)
        * [Proof points](/handbook/sales/command-of-the-message/#resources-core-content)
    * Value driver specific: offerings from our company or product that increase our value to a customer.
        * [Value Driver Framework](https://docs.google.com/document/d/1E0QM_dNhAo9bslmtTW1xGqnT9E3kbHBt7UhwGtdYIV8/edit)
* Build your sequence in Outreach or leverage existing content by cloning it and adding your voice, messaging approach, and/or industry language. Keep in mind your daily outbound metrics to make at least 30 emails and 30 dials per day so set your sequences up accordingly.  
* Sync with your SAL/AE and discuss what you have built as well as your strategy and timeline. 

**Step 4: First Touch**
* Initiate your sequence
* Always keep in mind the end recipient and place yourself in their shoes
* Offer value and assistance whether there is current demand or not

### Event Promotion and Follow Up Assistance
The SDR team will assist the Field Marketing and Corporate Marketing teams through event promotion and follow up. Learn more about [Field](/handbook/marketing/revenue-marketing/field-marketing/) or [Corporate Marketing](/handbook/marketing/corporate-marketing/) via their linked handbook pages. When these teams begin preparing for an event, they will create an issue using an issue template. Within that template is a section titled ‘Outreach and Follow Up’ for the SDR team to understand what is needed from them for each event. 
*  [Field Event Template](https://gitlab.com/gitlab-com/marketing/field-marketing/tree/master/.gitlab/issue_templates)
*  [Corporate Event Template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/Corporate-Event-Request.md)

#### Process
1. The Marketing Manager creaetes the issue and fill out the goals in the section, ‘Outreach and Follow Up’. They will also mention the appropriate SDR manager based on the region of the event or bandwidth.
1. The SDR manager will select one SDR rep from their team to be the project lead for this event and mention them in the issue.
1. The project lead will read through the goals outlined and begin completing them. 
1. Once an action is finished, the SDR rep will review it with their manager and then edit the description of the issue to link to the completed action item. 
1. The project lead will send the sequence to MktgOps or SDR Enablement to add both sequences to the Outreach collection.
1. SDR Manager and project lead will then connect on the date to launch the sequence and enable any other SDR reps assisting with this.
1. SDR project lead to announce the follow up sequence availability in the relevant SDR slack channels. 

#### Deadlines
**Event Promotion**
- The SDR manager needs to be assigned a minimum of 30-days prior to the event, but the sooner they can start prepping, the better.
- The SDR project lead will have one week to pull a list of people for event promotion and create or clone an event promotion sequence. The sequence will run for the next three weeks prior to the event. 

**Event Follow Up**
- The follow up sequence needs to be created at minimum of one week before the event date. 

**Sequence Information**
- Please clone one of the Master Event Sequences found in the [Events Outreach collection](https://app1a.outreach.io/sequences?direction=desc&order=recent&content_category_id%5B%5D=6) and pre populate as many of the variables as possible. If all of the variables can be populated manually or through Outreach/Salesforce, change the email type from ‘manual’ to ‘auto.’ Work with your manager and the FMM to make any other additional adjustments that may be needed.

## SDR Standards
* Meet monthly quota of Sales Accepted Opportunities (SAOs)
* Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
* Have a sense of urgency - faster response time directly influences conversion rates.
* Work lead records within Salesforce by leveraging sequences in Outreach
* Maintain a sense of ownership of data integrity in Salesforce and Outreach: cleaning up and assuring accuracy and consistency of data.
* Adding any information gathered about a LEAD, CONTACT, ACCOUNT from the data sources Datafox & DiscoverOrg.
* Attend each initial qualifying meeting (IQM) with your AE/SAL. Document notes and communicating with your AE/SAL after the meeting.

# Named SDR Team 
The Named SDR Team supports only Named SALs/Accounts. Their comp structure is based on SAOs and Qualified Meetings (IQMs) per month.

### Qualified Meeting

**What is considered a Qualified Meeting?**

A Qualified Meeting is a meeting that occurs with a prospect who is directly involved in a project or team related to the potential purchase of GitLab within a buying group, either as an evaluator, decision maker, technical buyer, or *influencer* within an SDR Target Account. 
To be considered qualified, the meeting must occur (prospect attends) and new information must be uncovered that better positions the SAL and Regional SDR to create a SAO with that particular account based on the SAO Criteria as stated in the handbook.

**Qualified Meeting Process**

A “New Event” IQM gives an SDR the ability to track every meeting that is scheduled and held in Salesforce without having to prematurely create a net new opportunity. SDRs can tie any event (IQM) to an existing account, opportunity, etc.The purpose of creating an IQM in salesforce is to provide the ability to track how many IQMs are being created per SDR and what the results are of these IQMs. 

**How to create an IQM in SFDC**

Within Salesforce there is a button titled “New Event” which you can access on your Salesforce “Home Screen”, and in the “Open Activity” section of any Lead, Contact, Account, and Opportunity record.

Under “Open Activity” on the Lead Record - Click on “New Event” button and you will be directed to the new event screen where you will see multiple fields to complete (see image below). Once a meeting with a prospect is confirmed, the SDR will send either a google calendar invite to the prospect (include their SAL) and/or send a meeting in Outreach.io, then create a new event IQM under the lead (or contact) record in Salesforce and include their Pre IQM notes in the Description field. Please note**If you create a meeting through Outreach.io, it will create a calendar event with all the info populated from your New Event IQM!! For a visual/video walkthrough of this process take a look [here](https://docs.google.com/document/d/1oB3RsjqCIUoXvX3F4DTrYOLh4X0K-7tzUPs3jYAQrwg/edit?usp=sharing). 

Once the IQM occurs, then the SDR can add their Post IQM notes and update the Event Disposition accordingly (If SAO, convert Lead to Contact record and create new opportunity with same process as outlined in Handbook or reschedule, etc.) 

**How to Create an IQM in Outreach**

In Outreach, you have the ability to book a meeting directly from a prospect overview. Even better, this can be done while you’re on a call with a prospect or immediately after without having to leave Outreach. Booking a meeting from Outreach will automatically pull the following fields directly into an SFDC event object:
* Subject
* Location (Zoom Link)
* Start & End times
* Assigned To
* Type
* Outreach Meeting Type
* Booked By
* Attributed Sequence Name

Select “book meeting” from the prospect actions menu. For a visual/video walkthrough of this process take a look [here](https://docs.google.com/document/d/1oB3RsjqCIUoXvX3F4DTrYOLh4X0K-7tzUPs3jYAQrwg/edit?usp=sharing).

### Named SDR Compensation
The Enterprise Named SDR team compensation is based on two key metrics: Qualified Meetings Occurred and Sales Accepted Opportunties (SAOs).
The Named SDR Team has a monthly goal for both Qualified Meeting (30%) and SAO (70%) component which works out to a 70/30 variable split. Accelerators will be consistent with those outlined in SDR Compensation and Quota. For accelerators, the individual quota is determined by each SDRs SAO attainment.

# Acceleration Marketing

The Acceleration team is responsible for generating awareness, qualified meetings, and pipeline. The team uses tactics like digital advertising, outbound prospecting, social selling, and creative outreach to evangelize and educate new prospects on what is possible with GitLab. Unlike the Regional SDR teams, the Acceleration team may not be tied to a particular territory, SAL, or AE. Instead, they focus wherever demand generation and pipeline is needed most.

## Expectations

*  Meet monthly Team Quota of Qualified Meetings
*  Exceptional Salesforce hygiene, logging all prospecting activity, opportunity creation
*  Maintain a high sense of autonomy to focus on what's most important; awareness and education
*  Continuously contribute to help the team improve quality and efficiency
*  Participate in planning, execution, and cross-functional meetings 
*  Participate in each initial qualifying meetings (IQM) generated - documenting notes and communicating with the SAL and Regional SDR after the meeting

## AMER Alignment | Large (Geo only)
The Enterprise AMER Acceleration team consists of two teams of three SDRs (Delta Team & Bravo Team). The teams are not tied to a particular region or territory. Instead, they rotate from Geo-to-Geo and focus where pipeline is needed most.

| Region         |  **Team**             | Acceleration SDR      | 
| :------------- |  :--------------------------------- | :----------------- | 
| AMER         |  Bravo Team   | Shawn Winters     | 
| AMER           |  Bravo Team           | Kevin McKinley    |
| AMER           |  Bravo Team          | David Fisher   |
| AMER         |  Delta Team   | Brandon Greenwell     | 
| AMER           |  Delta Team           | Matthew Beadle    |
| AMER           |  Delta Team          | Michael LeBeau  |

Note: The Acceleration AMER team **does not** cover Named Accounts, Regions, or SALs. Only Geo Regions.

## AMER Alignment | MidMarket

The MidMarketAMER Acceleration Team is coming and expected to be active by Q2FY21. The plan is to hire two Acceleration SDRs to focus exclusively on the MidMarket accounts in North America. One will be focused on Mid-Market East and the other on Mid-Market West. SMB Accounts & Regions will **not** be covered by Acceleration.

## EMEA Alignment | Large

SDRs on the EMEA Acceleration Team are each responsible for a specified Enterprise Geo or Geos (listed below) and will cycle through Target accounts on a weekly or monthly basis. The maximum number of active target accounts at one time is 200. Target accounts should be prioritized using the Net New Account Scoring and Prioritization Model and follow the Net New Outbound Framework.

| Region         |  **Sub-Region**             | Acceleration SDR      | 
| :------------- |  :--------------------------------- | :----------------- | 
| EMEA           |  France / Eastern Europe    | Anthony Seguillon     | 
| EMEA           |  Germany / CH/AUS           | Christina Souleles    |
| EMEA           |  Nordics / Benelux          | Goran Bijelic

Other EMEA Regions will be covered on a rolling basis. The team will work together to cover the region and regions will be cycled through monthly or bi-monthly.

## Working with Regional Sales and SDR Teams

Both Enterprise (Territory) Sales Reps and Regional SDR’s receive support from the Acceleration team. For Acceleration coverage, meeting cadence consists of the following:

*  **Initial Coverage Kick-off meeting**: Time - 30 minutes; Discuss tactics, target accounts, protected accounts, events, prospecting strategy, and schedules
*  **Weekly Update Meeting** : If necessary
*  **Coverage Recap Meeting**: Time 30 minutes; Discuss results, account intelligence, actionable insights, and collect feedback

Additional ad-hoc meetings may be scheduled in addition to the above.

The Regional SDR teams have a direct impact on the Acceleration SDR team’s success. It is important to set clear expectations and establish open lines of communication. SAL’s and SDR’s have important knowledge on the accounts and territory, so be sure to communicate regularly to ensure that you are all as effective as possible.

## Acceleration Marketing Compensation and Quota

The Acceleration team's compensation is based on one key metric: Team Qualified Meetings Occurred

It is important to note that the Acceleration Team has a monthly **team** goal for each geography (AMER & EMEA). Team members must work together to achieve the monthly Qualified Meeting number. Accelerators will be consistent with those outlined in [SDR Compensation and Quota](https://about.gitlab.com/handbook/marketing/revenue-marketing/sdr/#sdr-compensation-and-quota). For accelerators, the individual quota is determined by dividing the *team number* by the *total number of SDRs on the Geo team*.

**The Acceleration Marketing Team will also be measured on the following:**

* Results
  * Pipeline value of Acceleration-sourced opportunities
  * Progression of sourced opportunities beyond the Discovery Stage
* Activity
  * Number of meetings generated
  * Number of SAOs converted
  * Number of calls made
  * Number of personalized emails sent
  * Number of conversations

The above measurements do not impact team quota attainment but are critical to being successful as a member of the Acceleration Team. The team's focus should always be achieving the Qualified Meeting Quota.

**What is considered a Qualified Meeting?**

A Qualified Meeting is a meeting that occurs with an Acceleration Target Account. To be considered qualified, the meeting must **occur** and new information must be uncovered that better positions the SAL and Regional SDR to create a SAO with that particular account.

## Target Region Selection & Coverage Requests

The Acceleration Team Manager is responsible for determining Acceleration Execution Teams, Target Regions, and the Coverage Timeline. Target Regions will be prioritized based on:
1.  Sales Hiring Roadmap
2.  Regional Sales Director / E-Team Request
3.  Area Sales Manager Request
4.  Field Marketing Request
5.  SDR Leadership Request
6.  Field Sales Request
7.  Manager Discretion

If you would like to request Acceleration Coverage, please create an [Issue](https://gitlab.com/gitlab-com/marketing/sdr/issues) on using the `acceleration_coverage_request` template.

## Target Account Identification

Identifying *known* and *unknown* Target Accounts is an important first step in the Acceleration Coverage Process. To determine the target accounts for a region, the following process should be used:
1.  Identify *known* accounts from Salesforce.com
- `Pull Accounts by *Account Owner*` using [this report](https://gitlab.my.salesforce.com/00O4M000004dmXG)
- `Exclue ALL Customers`
- `Exclude SDR Targeted Accounts`
- `Exclude Accounts with Active Opportunities`

2.  Identify *unknown* accounts from DataFox
- `2000+ employees for Enterprise, 1000-1999 for Mid-Market`
- `Exclude Government / Public Sector`
- `Filter by Region` (State or Metro)
- `Exclude Existing Accounts` (via SFDC export and DataFox upload)
- `Match Subisidiaries`

3. Combine Salesforce & DataFox target account lists
4. Dedupe & Review
5. Share Target Account List with ABM team for Demandbase appending and targeting
6. List Match in DiscoverOrg (by company domain)
7. Execute saved Boolean searches and append prioritization data to target lists
8. Run CE Usage Reports & Append data
9. Divvy up target accounts 
10. Review final Target Account document with Regional SDR & SAL teams
11. Ensure SDR Targeted Accounts & SAL suppression lists are up-to-date
12. Finalize Target Account List
13. Pull target prospects (by titles & keywords) from DiscoverOrg
14. Submit Issue for Marketing Ops DiscoverOrg Prospect List upload

## Net New Account Scoring & Prioritization Model

The next step after identifying all of the Net New target accounts in a region is to prioritize them. The scoring model below should be used to determine the Net New Account Tier which will help guide prioritization and prospecting efforts using the Outbound Prospecting Framework (below).

**A Tier Accounts** - Have at least 4 out of 5 qualifiers below

**B Tier Accounts** - Have 3 out of 5 qualifiers below

**C Tier Accounts** - Have 2 out of 5 qualifiers below

**D Tier Accounts** - Have 1 out of 5 qualifiers below

**F Tier Accounts** - Have 0 out of 5 qualifiers OR zero direct revenue potential in the next 3 years

**Qualifiers:**
*  Current CE Usage
*  250+ employees in IT/TEDD positions
*  Good Fit Industry / Vertical (High Growth, Technology, Financial, Heathcare, Regulated Business)
*  Early Adopters / Innovative IT Shops (Identifiers & Keywords): Kubernetes / Containers, Microservices, Multi-cloud, DevOps, DevSecOps, CICD (and open-source + proprietary tools), SAST / DAST, Digital Transformation
*  Current DevOps Adoption (multiple DevOps roles on staff or hiring for multiple DevOps positions)

## Net New Outbound Prospecting Framework

### Tier A Target Accounts 

**Goal:** Conversations & MQLs 

**Priority:** High - 60% of focus

**Tactics:**
*  Hyper personalized outreach
*  Simulatneous targeting (multiple prospects targeted with different messaging)
*  Creative outbound
*  Direct mail
*  Targeted advertising
*  Pointdrive / Pathfactory / Outreach for low-level prospects to generate “groundswell” to improve engagement, uncover intent, and drive MQLs
*  Event Outreach

### Tier B Target Accounts 

**Goal:** Conversations & MQLs 

**Priority:** Medium - 30% of focus

**Tactics:**
*  Hyper personalized outreach
*  Simulatneous targeting (multiple prospects targeted with different messaging)
*  Creative outbound
*  Direct Mail
*  Role-, Persona-, or Keyword- based Outreach Sequences
*  Targeted advertising
*  Pointdrive / Pathfactory / Outreach for low-level prospects to generate “groundswell” to improve engagement, uncover intent, and drive MQLs
*  Event Outreach

### Tier C & D Target Accounts 

**Goal:** Conversations & MQLs 

**Priority:** Low - less than 10% of focus

**Tactics:**
*  Role-, Persona-, or Keyword- based Outreach Sequences
*  Simulatneous targeting (multiple prospects targeted with different messaging)
*  Pointdrive / Pathfactory to generate “groundswell” (improving engagement, uncovering intent, and driving MQLs)

### Tier F Target Accounts

**Goal:** Do not target and eliminate from Regional SDR & SAL Target Lists

**Priority:** less than 2%
        